const imageSmiley		= document.getElementById('smiley-image');
const imageDNF		= document.getElementById('dnf-image');
const imageTradi		= document.getElementById('tradi-image');
const imageMulti		= document.getElementById('multi-image');
const imageMystery	= document.getElementById('mystery-image');
const imageEarth		= document.getElementById('earth-image');
const imageBG		= document.getElementById('bg-image');
const imageMenu		= document.getElementById('menu-image');

const buttonStartWave = document.getElementById('btn-start-wave');
const buttonBuyTradi = document.getElementById('btn-buy-tradi');
const buttonBuyMulti = document.getElementById('btn-buy-multi');
const buttonBuyMystery = document.getElementById('btn-buy-mystery');
const buttonBuyEarth = document.getElementById('btn-buy-earth');

const chkShowAOE = document.getElementById('chk-show-aoe');

const textStatus = document.getElementById('text-status');

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const rounds_to_win = 10;
const X = 1;

const grid_width = 20;
const grid_height = 20;
const pixel_size = 40;

var negative_points	= 0;
var gold		= 50;

var hud_ticks = 0;
var hud_text = "";

const cost_pass = 20;
const max_count_enemies_pass = 30;

var grid = undefined;

var running = true;
var draw_grid = true;
var draw_grid_forbidden = false;
var draw_path = false;

var active_enemies	= [];
var active_towers	= [];
var active_bullets	= [];
var active_wave		= 1;

var tower_to_place = null;

var tick_time = 0;

var time_start = Date.now();
var time_end = 0;
var time_delta = 0;

const PHASE_PLAY	= 0;
const PHASE_PLACE	= 1;
const PHASE_MENU	= 2;

var current_phase = PHASE_PLAY;

class Game {

	constructor(rounds_to_win, start_gold, cost_per_pass, max_passed_enemies) {
		this.rounds_to_win	= rounds_to_win;
		this.negative_points	= 0;
		this.gold		= start_gold;
		this.cost_per_pass	= cost_per_pass;
		this.max_passed_enemies = max_passed_enemies;

		this.tick_time = 0;
		this.current_phase	= PHASE_MENU;
	}

	tick() {

	}

	draw() {

	}

}

var enemy_types = [
	// SPEED, HEALTH, IMG
	[3, 2, imageDNF],
	[4, 1, imageDNF],
	[2, 4, imageDNF],
	[1, 6, imageDNF],
	[2, 100, imageDNF]
];

var wave_0 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[0, 10],
	[1, 20],
	[0, 30],
	[0, 40]
];

var wave_1 = [
	// ENEMY TYPE, SPAWN AT TICKS
	[2, 10],
	/*[4, 15],
	[4, 20],
	[4, 25],
	[4, 30],
	[4, 35],
	[4, 40],
	[4, 45],
	[4, 50],
	[4, 55],
	[4, 60],
	[4, 65],
	[4, 70],
	[4, 75],*/
	/*[0, 80],
	[0, 90],
	[1, 100],
	[0, 110],
	[0, 120],
	[0, 130],
	[1, 140],
	[0, 150],
	[0, 160],
	[0, 170],
	[1, 180],
	[0, 190],
	[0, 200]*/
];

var waves = [
	wave_0,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
	wave_1,
];

function removeFromArray(array, index) {

	let before = array.slice(0, index);
	let after = [];

	if(index + 1 < array.length) {
		after = array.slice(index+1);
	}

	let both = before.concat(after);
	return both;
}

class Point {

	constructor(x, y) {
		this.x = x;
		this.y = y;
	}

}

class Path {

	constructor(point_start, ctrl_start, point_end, ctrl_end) {
		this.point_start = point_start;
		this.point_end = point_end;
		this.ctrl_start = ctrl_start;
		this.ctrl_end = ctrl_end;
	}

	getPosAt(progress) {

		var waypoint_start_ctrl = new Point(
			(this.ctrl_start.x - this.point_start.x)*progress + this.point_start.x,
			(this.ctrl_start.y - this.point_start.y)*progress + this.point_start.y
		);

		var waypoint_ctrl_ctrl = new Point(
			(this.ctrl_end.x - this.ctrl_start.x)*progress + this.ctrl_start.x,
			(this.ctrl_end.y - this.ctrl_start.y)*progress + this.ctrl_start.y
		);

		var waypoint_ctrl_end = new Point(
			(this.point_end.x - this.ctrl_end.x)*progress + this.ctrl_end.x,
			(this.point_end.y - this.ctrl_end.y)*progress + this.ctrl_end.y
		);



		var waypoint_startctrl_ctrlctrl = new Point(
			(waypoint_ctrl_ctrl.x - waypoint_start_ctrl.x)*progress + waypoint_start_ctrl.x,
			(waypoint_ctrl_ctrl.y - waypoint_start_ctrl.y)*progress + waypoint_start_ctrl.y
		);

		var waypoint_ctrlctrl_ctrlend = new Point(
			(waypoint_ctrl_end.x - waypoint_ctrl_ctrl.x)*progress + waypoint_ctrl_ctrl.x,
			(waypoint_ctrl_end.y - waypoint_ctrl_ctrl.y)*progress + waypoint_ctrl_ctrl.y
		);


		var waypoint_final = new Point(
			(waypoint_ctrlctrl_ctrlend.x - waypoint_startctrl_ctrlctrl.x)*progress + waypoint_startctrl_ctrlctrl.x,
			(waypoint_ctrlctrl_ctrlend.y - waypoint_startctrl_ctrlctrl.y)*progress + waypoint_startctrl_ctrlctrl.y
		);

		return waypoint_final;
	}

	getLength(iterations) {

		let last_point = this.point_start;
		let len = 0;

		for(let i = 1/iterations; i <= 1; i += 1/iterations) {
			let current_point = this.getPosAt(i);

			let x = current_point.x - last_point.x;
			let y = current_point.y - last_point.y;

			let amount = Math.sqrt(x*x + y*y);
			len += amount;

			last_point = current_point;
		}

		return len;
	}

}

class ComposedPath {

	constructor() {
		this.paths = [];
		this.path_sections = [];
		this.total_length = 0;
	}

	setPaths(paths) {
		this.paths = paths;
		this.path_sections = [];
		this.total_length = 0;

		for(let i = 0; i < paths.length; ++i) {
			this.total_length += paths[i].getLength(12);
		}

		for(let i = 0; i < paths.length; ++i) {

			let len = paths[i].getLength(12);
			let section_size = len;
			this.path_sections.push(section_size);
		}

		console.log(this.path_sections);
	}

	getPosAt(progress) {

		progress = Math.min(1, progress);

		let length_progress = this.total_length * progress;
		let lengths = 0;

		for(let i = 0; i < this.path_sections.length; ++i) {

			lengths += this.path_sections[i];

			if(length_progress <= lengths) {
				let overshoot = lengths - length_progress;
				let section_minus_overshoot = this.path_sections[i] - overshoot;

				let section_progress = section_minus_overshoot/this.path_sections[i];

				return this.paths[i].getPosAt(section_progress);
			}
		}

		return new Point(0, 0);
	}
}

class Tower {

	constructor() {
		this.draw_aoe = false;
	}

	draw() {
		console.log("stub");
	}

	tick() {

	}

	spawnBullet(x, y, target) {

		var bullet = new Bullet(x, y, target);
		active_bullets.push(bullet);

	}

	setDrawAOE(b) {
		this.draw_aoe = b;
	}

}

class TradiTower extends Tower {

	constructor(x, y) {
		super();
		this.img = imageTradi;
		this.grid_x = x;
		this.grid_y = y;
		this.radius = 150;
		this.ticks_to_rest = 0;
	}

	distanceToEnemy(enemy) {

		let my_x = this.grid_x*pixel_size - pixel_size/2;
		let my_y = this.grid_y*pixel_size - pixel_size/2;

		let enemy_pos = enemy.getPosition();

		let dx = enemy_pos.x - my_x;
		let dy = enemy_pos.y - my_y;

		return Math.sqrt((dx*dx) + (dy*dy));

	}

	getNearestEnemy() {

		let nearest_enemies = [];
		let distances = [];

		for(let i = 0; i < active_enemies.length; ++i) {

			let enemy = active_enemies[i];
			let distance = this.distanceToEnemy(enemy);

			if(distance <= this.radius) {
				nearest_enemies.push(enemy);
				distances.push(distance);
			}
		}

		let nearest_distance = 100000000;
		let nearest_enemy_index = -1;

		for(let i = 0; i < distances.length; ++i) {

			let enemy_distance = distances[i];

			if(enemy_distance < nearest_distance) {
				nearest_distance = enemy_distance;
				nearest_enemy_index = i;
			}
		}

		if(nearest_enemy_index >= nearest_enemies.length)
			return null;

		return nearest_enemies[nearest_enemy_index];
	}

	tick() {

		if(this.ticks_to_rest-- <= 0 && tick_time%5 == 0) {

			let enemy = this.getNearestEnemy();

			if(enemy != null) {
				this.spawnBullet(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemy);
				this.ticks_to_rest = 20;
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);

		if(this.draw_aoe) {
			ctx.fillStyle = '#FF000022';
			ctx.strokeStyle = '#FF000099';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, this.radius, 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
		}
	}

}


class MultiTower extends Tower {

	constructor(x, y) {
		super();
		this.img = imageMulti;
		this.grid_x = x;
		this.grid_y = y;
		this.radius = 200;
		this.ticks_to_rest = 0;
	}

	distanceToEnemy(enemy) {

		let my_x = this.grid_x*pixel_size - pixel_size/2;
		let my_y = this.grid_y*pixel_size - pixel_size/2;

		let enemy_pos = enemy.getPosition();

		let dx = enemy_pos.x - my_x;
		let dy = enemy_pos.y - my_y;

		return Math.sqrt((dx*dx) + (dy*dy));

	}

	getNearestEnemies3() {

		let out = [];

		let nearest_enemies = [];
		let nearest_distances = [];

		for(let i = 0; i < active_enemies.length; ++i) {

			let enemy = active_enemies[i];
			let distance = this.distanceToEnemy(enemy);

			if(distance <= this.radius) {
				nearest_enemies.push(enemy);
				nearest_distances.push(distance);
			}
		}


		for(let i = 0; i < 3; ++i) {

			let nearest_enemy_distance = 100000000;
			let nearest_enemy_index = -1;

			for(let k = 0; k < nearest_enemies.length; ++k) {

				let d = nearest_distances[k];

				if(d < nearest_enemy_distance) {
					nearest_enemy_distance = d;
					nearest_enemy_index = k;
				}
			}

			out.push(nearest_enemies[nearest_enemy_index]);

			nearest_enemies = removeFromArray(nearest_enemies, nearest_enemy_index);
			nearest_distances = removeFromArray(nearest_distances, nearest_enemy_index);
		}

		return out;
	}

	tick() {

		if(this.ticks_to_rest-- <= 0 && tick_time%5 == 0) {

			let enemies = this.getNearestEnemies3();

			//console.log(enemies.length);
			for(let i = 0; i < enemies.length; ++i) {
				if(enemies[i] != null) {
					this.spawnBullet(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemies[i]);
					this.ticks_to_rest = 60;

					console.log(enemies[i]);
				}
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);

		if(this.draw_aoe) {
			ctx.fillStyle = '#FF000022';
			ctx.strokeStyle = '#FF000099';
			ctx.lineWidth = 4;
			ctx.beginPath();
			ctx.arc(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, this.radius, 0, 2*Math.PI);
			ctx.fill();
			ctx.stroke();
		}
	}
}

class MysteryTower extends Tower {

	constructor(x, y) {
		super();
		this.img = imageMystery;
		this.grid_x = x;
		this.grid_y = y;
		this.radius = 0;
		this.ticks_to_rest = 0;
	}

	tick() {

		if(this.ticks_to_rest-- <= 0 && tick_time%5 == 0) {

			if(active_enemies.length > 0) {

				let enemy = active_enemies[0];

				if(tick_time >= enemy.spawn_time) {
					this.spawnBullet(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemy);
					this.ticks_to_rest = 15;
				}
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);
	}
}

class EarthTower extends Tower {

	constructor(x, y) {
		super();
		this.img = imageEarth;
		this.grid_x = x;
		this.grid_y = y;
		this.radius = 0;
		this.ticks_to_rest = 0;
		this.mode = 0;
	}

	tick() {

		if(this.ticks_to_rest-- <= 0 && tick_time%5 == 0) {

			for(let i = 0 ; i < active_enemies.length; ++i) {

				let enemy = active_enemies[i];

				this.spawnBullet(this.grid_x*pixel_size + pixel_size/2, this.grid_y*pixel_size + pixel_size/2, enemy);

				if(this.mode%5 == 0) {
					this.ticks_to_rest = 120;
				} else {
					this.ticks_to_rest = 5;
				}

				++this.mode;
			}
		}
	}

	draw() {
		ctx.drawImage(this.img, this.grid_x*pixel_size, this.grid_y*pixel_size, pixel_size, pixel_size);
	}

}

class Enemy {

	constructor(speed, health, img, spawn_time) {
		this.speed = speed;
		this.health = health;
		this.img = img;
		this.spawn_time = spawn_time;
		this.position_x = -1;
		this.position_y = -1;
		this.kill_radius = pixel_size;
		this.path_progression = 0;

		this.alive = true;
	}

	move() {
		this.position_x += this.dir_x*this.speed;
		this.position_y += this.dir_y*this.speed;
	}

	setPosition(x, y) {
		this.position_x = x;
		this.position_y = y;
	}

	getPosition() {
		return new Point(this.position_x - pixel_size/2, this.position_y - pixel_size/2);
	}

	removeHealth(amount) {
		this.health -= amount;

		if(this.health <= 0) {
			this.alive = false;
		}
	}

}

class Bullet {
	constructor(x, y, target) {
		this.position = new Point(x, y);
		this.target = target;
		this.radius = 5;
		this.speed = 15;
	}

	removeFromActive() {

		let index = active_bullets.indexOf(this);

		if(index >= 0) {

			let before = active_bullets.slice(0, index);
			let after = [];

			if(index + 1 < active_bullets.length) {
				after = active_bullets.slice(index+1);
			}

			let both = before.concat(after);
			active_bullets = both;
		}

	}

	tick() {

		if(this.target != null) {

			// do logic
			let vec_to_target = new Point(
				this.target.position_x - this.position.x,
				this.target.position_y - this.position.y,
			);

			let vec_len = Math.sqrt((vec_to_target.x*vec_to_target.x) + (vec_to_target.y*vec_to_target.y));
			vec_to_target.x /= vec_len;
			vec_to_target.y /= vec_len;

			this.position.x += vec_to_target.x*this.speed;
			this.position.y += vec_to_target.y*this.speed;

			if(vec_len <= this.target.kill_radius) {
				this.target.removeHealth(1);
				this.removeFromActive();

			}

		} else {

			// REMOVE BULLET

			let index = active_bullets.indexOf(this);

			if(index >= 0) {

				let before = active_bullets.slice(0, index);
				let after = [];

				if(index + 1 < active_bullets.length) {
					after = active_bullets.slice(index+1);
				}

				let both = before.concat(after);
				active_bullets = both;
			}

		}

	}

	draw() {
		ctx.beginPath();

		ctx.fillStyle = 'darkgray';
		ctx.strokeStyle = 'black';
		ctx.arc(this.position.x - this.radius/2, this.position.y - this.radius/2, this.radius, 0, 2*Math.PI);
		ctx.fill();
		ctx.stroke();
	}
}

var composed_path;

var paths = [

	new Path(
		new Point(-10, 340),
		new Point(20, 345),
		new Point(240, 340),
		new Point(220, 345),
	),new Path(
		 new Point(240, 340),
		 new Point(260, 330),
		 new Point(263, 313),
		 new Point(263, 323),
	),new Path(
		 new Point(263, 313),
		 new Point(263, 303),
		 new Point(259, 208),
		 new Point(259, 228),
	),new Path(
		 new Point(259, 208),
		 new Point(259, 187),
		 new Point(234, 178),
		 new Point(250, 178),
	),new Path(
		 new Point(234, 178),
		 new Point(214, 178),
		 new Point(170, 178),
		 new Point(190, 178),
	),new Path(
		 new Point(170, 178),
		 new Point(150, 178),
		 new Point(139, 208),
		 new Point(139, 190),
	),new Path(
		 new Point(139, 208),
		 new Point(139, 250),
		 new Point(142, 550),
		 new Point(142, 500),
	),new Path(
		 new Point(142, 550),
		 new Point(142, 570),
		 new Point(112, 579),
		 new Point(132, 579),
	),new Path(
		 new Point(112, 579),
		 new Point(100, 579),
		 new Point(51, 582),
		 new Point(70, 582),
	),new Path(
		 new Point(51, 582),
		 new Point(30, 582),
		 new Point(19, 550),
		 new Point(19, 570),
	),new Path(
		 new Point(19, 550),
		 new Point(19, 530),
		 new Point(19, 492),
		 new Point(19, 510),
	),new Path(
		 new Point(19, 492),
		 new Point(19, 470),
		 new Point(49, 460),
		 new Point(30, 460),
	),new Path(
		 new Point(49, 460),
		 new Point(90, 460),
		 new Point(311, 458),
		 new Point(200, 458),
	),new Path(
		 new Point(311, 458),
		 new Point(331, 458),
		 new Point(340, 429),
		 new Point(340, 459),
	),new Path(
		 new Point(340, 429),
		 new Point(340, 400),
		 new Point(339, 251),
		 new Point(339, 271),
	),new Path(
		 new Point(339, 251),
		 new Point(339, 221),
		 new Point(372, 221),
		 new Point(352, 221),
	),new Path(
		 new Point(372, 221),
		 new Point(392, 221),
		 new Point(472, 222),
		 new Point(452, 222),
	),new Path(
		 new Point(472, 222),
		 new Point(492, 222),
		 new Point(504, 252),
		 new Point(504, 222),
	),new Path(
		 new Point(504, 252),
		 new Point(504, 292),
		 new Point(502, 544),
		 new Point(502, 500),
	),new Path(
		 new Point(502, 544),
		 new Point(502, 574),
		 new Point(532, 581),
		 new Point(502, 581),
	),new Path(
		 new Point(532, 581),
		 new Point(552, 581),
		 new Point(631, 581),
		 new Point(601, 581),
	),new Path(
		 new Point(631, 581),
		 new Point(651, 581),
		 new Point(661, 551),
		 new Point(661, 571),
	),new Path(
		 new Point(661, 551),
		 new Point(661, 521),
		 new Point(662, 488),
		 new Point(662, 510),
	),new Path(
		 new Point(662, 488),
		 new Point(662, 450),
		 new Point(629, 458),
		 new Point(650, 458),
	),new Path(
		 new Point(629, 458),
		 new Point(600, 458),
		 new Point(450, 458),
		 new Point(480, 458),
	),new Path(
		 new Point(450, 458),
		 new Point(430, 458),
		 new Point(421, 485),
		 new Point(421, 455),
	),new Path(
		 new Point(421, 485),
		 new Point(421, 520),
		 new Point(420, 670),
		 new Point(420, 630),
	),new Path(
		 new Point(420, 670),
		 new Point(420, 690),
		 new Point(450, 701),
		 new Point(430, 701),
	),new Path(
		 new Point(450, 701),
		 new Point(490, 701),
		 new Point(797, 701),
		 new Point(700, 701),
	)
];


function createGrid() {

	grid = [
		[X,X,X,0,0,0,0,0,0,0,0,0,X,X,X,X,X,X,X,X],
		[X,X,0,0,0,0,0,0,0,0,0,X,X,X,0,0,X,X,X,X],
		[X,X,0,0,0,0,0,0,0,0,0,X,X,0,0,0,0,0,X,X],
		[X,0,0,0,0,0,0,0,0,0,0,X,X,0,0,0,0,0,X,X],
		[0,0,0,X,X,X,X,0,0,0,0,X,X,0,0,0,0,0,0,X],
		[0,0,0,X,0,0,X,0,X,X,X,X,X,0,0,0,0,0,0,X],
		[X,X,0,X,0,0,X,0,X,0,0,0,X,X,0,0,0,0,0,X],
		[X,X,0,X,0,0,X,0,X,0,0,0,X,X,X,0,0,0,0,X],
		[X,X,X,X,X,X,X,0,X,X,0,0,X,X,X,0,0,0,0,0],
		[0,0,0,X,0,0,X,X,X,X,X,0,X,0,X,X,0,0,0,0],
		[0,0,0,X,0,X,X,0,X,X,X,0,X,0,X,X,0,0,0,0],
		[X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,0,0,0],
		[X,0,0,X,0,X,X,0,0,X,X,0,X,0,X,X,X,0,0,0],
		[X,0,0,X,0,0,X,X,0,X,X,X,X,X,X,0,X,0,0,X],
		[X,X,X,X,0,0,X,X,0,0,X,X,X,X,X,X,X,0,0,X],
		[0,0,0,0,0,0,0,X,0,0,X,0,0,0,0,0,0,0,X,X],
		[X,X,0,0,0,0,X,X,0,0,X,0,0,0,0,0,0,0,0,X],
		[X,X,X,X,X,X,X,0,0,0,X,X,X,X,X,X,X,X,X,X],
		[X,X,0,0,X,X,X,X,0,0,0,0,0,0,0,0,0,0,0,0],
		[X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X,X],
	];
}

function drawGrid() {
	for(let y = 0; y < grid_height; ++y) {
		for(let x = 0; x < grid_width; ++x) {

			if(grid[y][x] == 0) {
				ctx.lineWidth = 1;
				ctx.strokeStyle = "green";
				ctx.strokeRect(x*pixel_size, y*pixel_size, pixel_size, pixel_size);
			} else if(draw_grid_forbidden) {
				ctx.fillStyle = "#FF000055";
				ctx.fillRect(x*pixel_size, y*pixel_size, pixel_size, pixel_size);
			}
		}
	}
}

function spawnEnemies(wave_array) {

	active_enemies = [];

	for(let i = 0; i < wave_array.length; ++i) {

		var enemy_stub = enemy_types[wave_array[i][0]];
		active_enemies.push(new Enemy(enemy_stub[0], enemy_stub[1], enemy_stub[2], wave_array[i][1]));
	}
}

function payForTower(tower, price) {
	if(gold >= price) {

		tower_to_place = tower;

		tower_to_place.setDrawAOE(true);
		setShowAOE(true);


		gold -= price;

		setHUDText("Turm gekauft!", 50);
	} else {
		setHUDText("Nicht genug Gold!", 50);
	}
}

function buyTower(type) {

	if(tower_to_place != null) {
		setHUDText("Du hast noch einen Turm!", 50);
		return;
	}

	if(type == "Tradi") {

		payForTower(new TradiTower(0, 0), 50);

	} else if(type == "Multi") {

		payForTower(new MultiTower(0, 0), 100);

	} else if(type == "Mystery") {

		payForTower(new MysteryTower(0, 0), 200);

	} else if(type == "Earth") {

		payForTower(new EarthTower(0, 0), 500);
	}
}

function setHUDText(text, ticks) {
	hud_text = text;
	hud_ticks = ticks;
}

function addGold(amount) {

	let g = gold + amount;
	gold = Math.max(0, g);
}

function setShowAOE(b) {
	for(let i = 0; i < active_towers.length; ++i) {
		active_towers[i].setDrawAOE(b);
	}
}

function evaluateTowers() {

	let k = active_towers.length;

	for(let i = 0; i < k; ++i) {

		let tower = active_towers[i];
		tower.tick();
	}

}

function evaluateBullets() {

	let k = active_bullets.length;
	for(let i = 0; i < k; ++i) {

		let bullet = active_bullets[i];

		if(bullet != null) {
			bullet.tick();
		}

	}

}

function evaluateEnemies() {

	for(let i = 0; i < active_enemies.length; ++i) {

		var enemy = active_enemies[i];

		if(tick_time >= enemy.spawn_time) {


			if(enemy.alive) {

				if(enemy.path_progression > 1) {
					++negative_points;

					addGold(-cost_pass);

					enemy.alive = false;
					continue;
				}

				enemy.path_progression += enemy.speed/composed_path.total_length;
				setHUDText(time_delta, 1);

				var pos_point = composed_path.getPosAt(enemy.path_progression);

				enemy.setPosition(pos_point.x, pos_point.y);

			} else {

				let before_enemies = active_enemies.slice(0, i);
				let after_enemies = [];

				if(i+1 < active_enemies.length) {
					after_enemies = active_enemies.slice(i+1);
				}

				let without_enemies = before_enemies.concat(after_enemies);
				active_enemies = without_enemies;
			}

		}

	}
}

function drawEnemies() {

	for(let i = 0; i < active_enemies.length; ++i) {

		var enemy = active_enemies[i];
		if(enemy.alive) {
			var pos = enemy.getPosition();
			ctx.drawImage(enemy.img, pos.x, pos.y, pixel_size, pixel_size);
		}
	}

}

function drawTowers() {

	for(let i = 0; i < active_towers.length; ++i) {

		let tower = active_towers[i];
		tower.draw();
	}

}

function drawBullets() {

	let k = active_bullets.length;
	for(let i = 0; i < k; ++i) {

		let bullet = active_bullets[i];
		bullet.draw();

	}
}

function drawHUD() {

	if(hud_ticks-- > 0) {
		ctx.fillStyle = 'white';
		ctx.strokeStyle = 'black';
		ctx.font = '50px sans';
		ctx.lineWidth = 2;

		let x = 10;
		let y = 50;

		ctx.fillText(hud_text, x, y);
		ctx.strokeText(hud_text, x, y);
	}
}

function drawPath(path) {

	ctx.fillStyle = 'green';
	ctx.fillRect(path.ctrl_start.x, path.ctrl_start.y, 10, 10);

	ctx.fillStyle = 'red';
	ctx.fillRect(path.point_start.x, path.point_start.y, 10, 10);

	ctx.fillStyle = 'blue';
	ctx.fillRect(path.ctrl_end.x, path.ctrl_end.y, 10, 10);

	ctx.fillStyle = 'orange';
	ctx.fillRect(path.point_end.x, path.point_end.y, 10, 10);

	let iterations = 12;
	let last_point = path.point_start;


	for(let i = 1/iterations; i <= 1; i += 1/iterations) {
		let current_point = path.getPosAt(i);

		ctx.strokeStyle = 'orange';
		ctx.lineWidth = 1;

		ctx.beginPath();
		ctx.moveTo(last_point.x, last_point.y);
		ctx.lineTo(current_point.x, current_point.y);
		ctx.stroke();

		//console.log("c: " + current_point.x + ", " + current_point.y);

		last_point = current_point;
	}
}

function drawWave() {

	var str = "Welle " + active_wave + "/" + rounds_to_win + "    Gold " + gold + "   Leben " + (max_count_enemies_pass-negative_points);

	ctx.font = '30px monospace';

	ctx.strokeStyle = 'black';
	ctx.lineWidth = 3;
	ctx.strokeText(str, canvas.width - 610, canvas.height - 10);

	ctx.fillStyle = 'white';
	ctx.fillText(str, canvas.width - 610, canvas.height - 10);

}

function grayButtons() {
	buttonStartWave.setAttribute('disabled', 'true');
}

function ungrayButtons() {
	buttonStartWave.removeAttribute('disabled');
}

function transition_to_phase(phase) {

	current_phase = phase;

	if(current_phase == PHASE_PLAY) {

		textStatus.innerText = "Viel Glück!";

		draw_grid = false;
		draw_grid_forbidden = false;
		draw_path = false;
		running = true;


		// Reset Rest
		for(let i = 0; i < active_towers.length; ++i) {
			let t = active_towers[i];
			t.ticks_to_rest = 0;
		}


		// GRAY ALL BUTTONS

		grayButtons();

		loadWave(active_wave);

	} else if(current_phase == PHASE_PLACE) {

		textStatus.innerText = "Aufbauphase";

		draw_grid = true;
		draw_grid_forbidden = false;
		draw_path = false;
		running = true;
		tower_to_place = null;

		ungrayButtons();

		active_bullets = [];
		active_enemies = [];

		// UNGRAY ALL BUTTONS

	} else if(current_phase == PHASE_MENU) {

		textStatus.innerText = "";

		draw_grid = false;
		draw_grid_forbidden = false;
		draw_path = false;
		running = true;

		tick_time = 0;

		active_wave	= 1;
		active_enemies	= [];
		active_towers	= [];

		// GRAY ALL BUTTONS

		grayButtons();

	}
}

function loadWave(wave_index) {

	tick_time = 0;
	active_wave = wave_index;
	spawnEnemies(waves[active_wave]);
}

function main() {

	composed_path = new ComposedPath();
	composed_path.setPaths(paths);

	createGrid();

	transition_to_phase(PHASE_PLACE);
	window.requestAnimationFrame(loop);
}

function loop() {


	time_end = Date.now();
	time_delta = time_end - time_start;

	let fps = 1000/100;

	if(time_delta > fps) {

		if(current_phase == PHASE_PLAY) {

			if(active_enemies.length > 0) {

				evaluateTowers();
				evaluateBullets();
				evaluateEnemies();
				++tick_time;

				addGold(1*(tick_time%8==0));

			} else {

				if(max_count_enemies_pass - negative_points > 0) {

					setHUDText("Welle geschafft!", 100);

					addGold(active_wave*10);

					if(active_wave == rounds_to_win) {

						transition_to_phase(PHASE_MENU);

					} else {

						++active_wave;
						transition_to_phase(PHASE_PLACE);

					}

				} else {

					// GAME LOST.
					transition_to_phase(PHASE_MENU);

				}

			}

			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(imageBG, 0, 0);

			if(draw_grid) drawGrid();

			drawEnemies();
			drawTowers();
			drawBullets();
			drawWave();
			drawHUD();

		} else if(current_phase == PHASE_PLACE) {

			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(imageBG, 0, 0);


			if(tower_to_place != null) {
				tower_to_place.draw();

				let x = tower_to_place.grid_x;
				let y = tower_to_place.grid_y;

				if(grid[y][x] == X) {

					draw_grid_forbidden = true;

				} else {

					draw_grid_forbidden = false;

				}
			}

			if(draw_grid) drawGrid();

			drawTowers();
			drawWave();
			drawHUD();

			// show tower selection
			// enable tower hover to replace
			// show tower ranges on hover


		} else if(current_phase == PHASE_MENU) {

			// MENU LOGIC

			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(imageMenu, 0, 0);


		}


		if(draw_path) {
			for(let i = 0; i < paths.length; ++i) {
				drawPath(paths[i]);
			}
		}


		time_start = time_end - (time_delta % fps);
	}


	if(running) window.requestAnimationFrame(loop);
}

canvas.addEventListener('mousemove', function(e) {
	if(tower_to_place != null) {
		tower_to_place.grid_x = Math.floor(e.layerX/pixel_size);
		tower_to_place.grid_y = Math.floor(e.layerY/pixel_size);
	}
});

canvas.addEventListener('click', function(e) {
	if(tower_to_place != null) {

		if(grid[tower_to_place.grid_y][tower_to_place.grid_x] != X) {

			active_towers.push(tower_to_place);
			tower_to_place = null;

			setShowAOE(chkShowAOE.checked);

		} else {

			setHUDText("Hier nicht platzierbar!", 60);

		}
	}
});

chkShowAOE.addEventListener('click', function(e) {

	if(chkShowAOE.checked) {
		setShowAOE(true);

	} else {
		setShowAOE(false);
	}
});

buttonStartWave.onclick = function(e) {
	transition_to_phase(PHASE_PLAY);
}

buttonBuyTradi.onclick = function(e) {
	buyTower("Tradi");
}

buttonBuyMulti.onclick = function(e) {
	buyTower("Multi");
}

buttonBuyMystery.onclick = function(e) {
	buyTower("Mystery");
}

buttonBuyEarth.onclick = function(e) {
	buyTower("Earth");
}

document.addEventListener('DOMContentLoaded', function() {
	main();
});
