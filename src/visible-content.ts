
let visibleContentColumn = document.getElementById('visible-content-column')!!;

export function clearColumn() {
    visibleContentColumn.innerHTML = "";
}

export function addRow(content:HTMLElement) {
    let row = document.createElement('div');
    row.classList.add('visible-content-row');

    row.appendChild(content);
    visibleContentColumn.appendChild(row);
}