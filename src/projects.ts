type Lang = {
    color:string;
    display:string;
}

interface StringIndexedDictionary {
    [index:string]:Lang;
}

let Language = {
    c: {
        color: "#555555",
        display: "C"
    },
    cpp: {
        color: "#f34b7d",
        display: "C++"
    },
    csharp: {
        color: "#178600",
        display: "C#"
    },
    cobol: {
        color: "black",
        display: "Cobol"
    },
    go: {
        color: "#00ADD8",
        display: "Go"
    },
    java: {
        color: "#b07219",
        display: "Java"
    },
    javascript: {
        color: "#f1e05a",
        display: "JavaScript"
    },
    kotlin: {
        color: "#A97BFF",
        display: "Kotlin"
    },
    php: {
        color: "#4F5D95",
        display: "PHP"
    },
    python: {
        color: "#3572A5",
        display: "Python"
    },
    typescript: {
        color: "#3178c6",
        display: "TypeScript"
    },
    r: {
        color: "#198CE7",
        display: "R"
    },
    rust: {
        color: "#dea584",
        display: "Rust"
    },
    scala: {
        color: "#c22d40",
        display: "Scala"
    },
    zig: {
        color: "#ec915c",
        display: "Zig"
    }

} as StringIndexedDictionary;

type Project = {
    title:string;
    description:string;
    img:string;
    language:string;
    gitlab:string;
    demo:string;
    images:string[];
}

function generateProjectWidget(project:Project):HTMLElement {

    let projectWidget = document.createElement('div');
    projectWidget.classList.add('project-widget');

        let projectWidgetHeader = document.createElement('div');
        projectWidgetHeader.classList.add('project-widget-header');

            let projectWidgetHeaderBackground = document.createElement('div');
            projectWidgetHeaderBackground.classList.add('project-widget-header-background');
            projectWidgetHeaderBackground.style.backgroundImage = `url('${project.img}')`;

            let projectWidgetHeaderTitle = document.createElement('div');
            projectWidgetHeaderTitle.classList.add('project-widget-header-title');
            projectWidgetHeaderTitle.innerText = project.title;

            projectWidgetHeader.appendChild(projectWidgetHeaderBackground);
            projectWidgetHeader.appendChild(projectWidgetHeaderTitle);

        let projectWidgetStrip = document.createElement('div');
        projectWidgetStrip.classList.add('project-widget-strip');
        projectWidgetStrip.setAttribute('title', Language[project.language].display);
        projectWidgetStrip.style.backgroundColor = Language[project.language].color;

        let projectWidgetPreviewImages = document.createElement('div');
        if (project.images !== undefined && project.images != null) {
            projectWidgetPreviewImages.classList.add('project-widget-preview')

            
                for (const image of project.images) {
                    let imageContainer = document.createElement('div');
                    imageContainer.classList.add('project-widget-preview-image-container');

                        let img = document.createElement('div');
                        //img.setAttribute('src', image);
                        img.classList.add('project-widget-preview-image');
                        img.style.backgroundImage = `url('${image}')`;
                        img.addEventListener('click', () => {
                            window.open(image, '_blank');
                        });

                        imageContainer.appendChild(img);
                        
                    projectWidgetPreviewImages.appendChild(imageContainer);
                }
        }

        let projectWidgetContent = document.createElement('div');
        projectWidgetContent.classList.add('project-widget-content');

            let projectWidgetContentDescription = document.createElement('div');
            projectWidgetContentDescription.classList.add('project-widget-content-description');
            projectWidgetContentDescription.innerHTML = project.description;

            let projectWidgetContentFooter = document.createElement('div');
            projectWidgetContentFooter.classList.add('project-widget-content-footer');

                let ul = document.createElement('ul');

                    if (project.gitlab !== undefined && project.gitlab != null) {
                        let li = document.createElement('li');
                        li.setAttribute('title', 'View on gitlab.com');
                        
                            let a = document.createElement('a');
                            a.setAttribute('target', '_blank');
                            a.setAttribute('href', project.gitlab);
                            a.innerHTML = `<img src="resources/gitlab-logo-700.svg" style="height: 30px; width: 30px;" alt="">`;
                        
                            li.appendChild(a);

                        ul.appendChild(li);
                    }

                    if (project.demo !== undefined && project.demo != null) {
                        let li = document.createElement('li');
                        li.setAttribute('title', 'Live demo');
                        
                            let a = document.createElement('a');
                            a.setAttribute('target', '_blank');
                            a.setAttribute('href', project.demo);
                            a.innerHTML = `<img src="resources/play-icon.png" style="height: 30px; width: 30px;" alt="">`;
                        
                            li.appendChild(a);
                            
                        ul.appendChild(li);
                    }

                projectWidgetContentFooter.appendChild(ul);
            
            projectWidgetContent.appendChild(projectWidgetContentDescription);
            projectWidgetContent.appendChild(projectWidgetContentFooter);

        projectWidget.appendChild(projectWidgetHeader);
        projectWidget.appendChild(projectWidgetStrip);

        projectWidget.appendChild(projectWidgetPreviewImages);
        projectWidget.appendChild(projectWidgetContent);
    
    return projectWidget;
}

function buildBlurBoxFromProjectWidget(projectWidget:HTMLElement):HTMLElement {
    let blurBox = document.createElement('div');
    blurBox.classList.add('blur-box');

    let copyOfWidget = projectWidget.cloneNode(true);
    (copyOfWidget as HTMLElement).classList.add('blurred');

    blurBox.appendChild(copyOfWidget);
    blurBox.appendChild(projectWidget);

    return blurBox;
}

export function generateProjects():HTMLElement {

    let projectList = document.createElement('div');

    fetch('resources/projects.json', {cache: "reload"}).then(result => result.json()).then(json => {
        json.forEach((project:Project) => {
            let projectWidget = generateProjectWidget(project);
            //let blurBox = buildBlurBoxFromProjectWidget(projectWidget);
            projectList.appendChild(projectWidget);
        });
    });

    return projectList;
}

