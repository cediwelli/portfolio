
class Star {

    private position:{x:number, y:number};

    private sizeMax:number;
    private sizeMin:number;
    private size:number;
    private sizeDecrease:number;

    private tickPause:number;
    private currentTicks:number;

    constructor() {
        this.position = {x: 0, y: 0};
        this.size = 1;
        this.sizeMax = 1;
        this.sizeMin = 0;
        this.tickPause = 4;
        this.currentTicks = 0;
        this.sizeDecrease = 0.1;
    }

    public setPosition(x:number, y:number):void {
        this.position = {x: x, y: y};
    }

    public getPosition():{x:number, y:number} {
        return this.position;
    }

    public setRandomPosition(xmin:number, xmax:number, ymin:number, ymax:number) {
        let randX = Math.random() * (xmax - xmin) + xmin;
        let randY = Math.random() * (ymax - ymin) + ymin;
        this.setPosition(randX, randY);
    }

    public setRandomSize(sizemin:number, sizemax:number) {
        this.size = Math.random() * (sizemax - sizemin) + sizemin;
        this.sizeMin = sizemin;
        this.sizeMax = sizemax;
    }

    public setRandomSizeDecrease(min:number, max:number) {
        this.sizeDecrease = Math.random() * (max - min) + min;
    }

    public setRandomTickPause(min:number, max:number) {
        this.tickPause = Math.round(Math.random() * (max - min) + min);
    }

    public tickSize() {
        if (this.currentTicks % this.tickPause == 0) {
            this.size -= this.sizeDecrease;
            if (this.size <= this.sizeMin - 1) {
                this.size = this.sizeMax;
            }
        }
        this.currentTicks++;
    }

    public getSize():number {
        return this.size;
    }

}

const canvas = document.getElementById('stars-canvas') as HTMLCanvasElement;
const canvasContainer = canvas.parentElement;
const ctx = canvas?.getContext("2d");
var timeout = 40;

var stars:Star[] = [];

function onNextFrame() {
    if (ctx == null || canvasContainer == null) {
        return;
    }
    canvas.setAttribute('width', `${canvasContainer.offsetWidth}`);
    canvas.setAttribute('height', `${canvasContainer.offsetHeight}`);
    renderStars(ctx);
}

function generateStars(amount:number, xmin:number, xmax:number, ymin:number, ymax:number) {
    for (let i = 0; i < amount; ++i) {
        let star = new Star();
        star.setRandomPosition(xmin, xmax, ymin, ymax);
        star.setRandomSize(1, 2);
        star.setRandomTickPause(3, 6);
        star.setRandomSizeDecrease(0.1, 0.15);
        stars.push(star);
    }
}

function startStars(amount:number, xmin:number, xmax:number, ymin:number, ymax:number):void {
    generateStars(amount, xmin, xmax, ymin, ymax);
    window.requestAnimationFrame(onNextFrame);
}

function renderStars(ctx:CanvasRenderingContext2D) {

    ctx.clearRect(0, 1000, 0, 1000);
    ctx.fillStyle = "white";
    
    for (const star of stars) {
        star.tickSize();

        let pos = star.getPosition();
        let size = star.getSize();

        ctx.beginPath();
        ctx.arc(pos.x, pos.y, size, 0, 2 * Math.PI);
        ctx.fillStyle = "white";
        ctx.fill();
    }

    setTimeout(() => {
        window.requestAnimationFrame(onNextFrame);
    }, timeout); 
}

window.addEventListener('DOMContentLoaded', () => {
    if (ctx == null || canvasContainer == null) {
        return;
    }

    canvas.setAttribute('width', `${canvasContainer.offsetWidth}`);
    canvas.setAttribute('height', `${canvasContainer.offsetHeight}`);

    let count = window.innerWidth / 10;
    startStars(count, 0, canvas.width, 0, canvas.height);
});

window.addEventListener('resize', () => {
    stars = [];
    let count = window.innerWidth / 10;
    generateStars(count, 0, canvas.width, 0, canvas.height);
});