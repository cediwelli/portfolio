type ResumeEntry = {
    title:string;
    description:string;
    start:number;
    end:number;
}

type Resume = {
    start:number;
    end:number;
    entries:ResumeEntry[];
}

type InfoBox = {
    infoBox:HTMLElement;
    infoBoxTitle:HTMLElement;
    infoBoxDescription:HTMLElement
}

export let demoResume = {
    start: 2018,
    end: 2024,
    entries: [
        {
            title: "Abitur",
            description: "<div>On June 22, 2018 I attained the Allgemeine Hochschulzugangsberechtigung from the Sopie-Scholl Gesamtschule in Wennigsen. The education I received furthered my already high interests in the STEM fields even further.</div> <img src=\"resources/sophiescholl.png\" style=\"height: 200px\">",
            start: 2015,
            end: 2018
        },
        {
            title: "Freiwilliger Wehrdienst",
            description: "After completing my Abitur, I took some time to <i>carefully</i> consider my <u>career path</u>. Rather than staying idle at home, I made the decision to join the <b>Bundeswehr</b> for a limited amount of time. During this period, I had the opportunity to contemplate various fields of study available to me. My primary interests during this phase included <i>Chemistry, Physics, and Legal Studies</i>. Despite having delved into programming since the age of 14, I was unaware that I could turn one of my favorite hobbies into a career at the university level.",
            start: 2018,
            end: 2019
        },
        {
            title: "Computer Science, B.Sc.",
            description: "<div>From Oct 2019 to Oct 2023 I studied Computer Science for my bachelor's degree at the Gottfried Wilhelm Leibniz University in Hannover. Throughout this period, I had the privilege of learning from distinguished professors in the field of computer science, gaining insights into various topics. I directed my attention towards low-level systems, operating systems, and the intricacies of software engineering.<br><br>Grade of bachelor's thesis: 1.0</div><img src=\"resources/logo_luh_sw.png\" style=\"height: 200px\"/>",
            start: 2019,
            end: 2023
        },
        {
            title: "Student Assistant, Software Engineering Department",
            description: "From 2022 on, I was hired as a student assistant at the institute of practical computer science for the software engineering department. In the first year, my responsibilities included checking students' homework and conducting both in-person and online exercises. Due to excellent performance, I was allowed to lead the lecture exercises as the first student assistant in the history of the Software Engineering department in the second year.",
            start: 2022,
            end: 2025
        },
        {
            title: "Computer Science, M.Sc.",
            description: "<div>I am currently studying for my masters degree in computer science, also at the Gottfreid Wilhelm Leibniz University in Hannover. After learning about the fundamentals of software engineering and systems engineering, I am motivated to learn more about these topics.</div><img src=\"resources/logo_luh_sw.png\" style=\"height: 200px\"/>",
            start: 2023,
            end: 2025
        }
    ]
};

class YearRow {

    private year:number;
    private infoBox:InfoBox;

    private activityEndRow:HTMLTableRowElement;
    private activityStartRow:HTMLTableRowElement;

    constructor(year:number, infoBox:InfoBox) {
        this.year = year;
        this.infoBox = infoBox;

        this.activityEndRow = document.createElement('tr');
        this.activityStartRow = document.createElement('tr');

        let emptyTd = document.createElement('td');
        emptyTd.classList.add('empty-td');

        let yearTd = document.createElement('td');
        yearTd.innerText = `${this.year}`;

        this.activityEndRow.appendChild(emptyTd);
        this.activityStartRow.appendChild(yearTd);
    }

    public startActivity(resumeEntry:ResumeEntry, isOpenTop:boolean, isOpenBottom:boolean) {
        let entry = document.createElement('td');

        let rowspan = (resumeEntry.end - this.year) * 2;
        if (rowspan == 0) {
            rowspan = 1;
        }
        
        entry.innerHTML = resumeEntry.title;
        entry.setAttribute('rowspan', `${rowspan}`);

        entry.addEventListener('click', () => {
            this.infoBox.infoBoxTitle.innerHTML = resumeEntry.title;
            this.infoBox.infoBoxDescription.innerHTML = resumeEntry.description;
            window.location.href = "#info-box";
        });

        if (!isOpenTop) {
            if (isOpenBottom) {
                entry.classList.add('open-bottom');
            }
            this.activityStartRow.appendChild(entry);
        } else {
            entry.classList.add('open-top');
            this.activityEndRow.appendChild(entry);
        }
    }

    public buildInto(table:HTMLTableElement) {
        table.appendChild(this.activityEndRow);
        table.appendChild(this.activityStartRow);
    }

}

function generateInfoBox():InfoBox {
    let infoBox = document.createElement('div');
    infoBox.setAttribute('id', 'info-box');

    let infoBoxTitle = document.createElement('div');
    infoBoxTitle.setAttribute('id', 'info-box-title');

    let infoBoxDescription = document.createElement('div');
    infoBoxDescription.setAttribute('id', 'info-box-description');

    infoBox.appendChild(infoBoxTitle);
    infoBox.appendChild(infoBoxDescription);

    return {
        infoBox: infoBox,
        infoBoxTitle: infoBoxTitle,
        infoBoxDescription: infoBoxDescription
    }
}

export function generateResumeTableHTML(json:Resume, infoBox:InfoBox):HTMLElement {

    //let infoBox = generateInfoBox();

    let table = document.createElement('table');
    let rows:Map<number, YearRow> = new Map();

    for (let year = json.start; year <= json.end; ++year) {
        rows.set(year, new YearRow(year, infoBox));
    }

    for (const resumeEntry of json.entries) {
        let yearRow = rows.get(resumeEntry.start);
        
        if (yearRow === undefined) {
            yearRow = rows.get(json.start);
            if (yearRow === undefined) {
                continue;
            }
        }

        yearRow.startActivity(resumeEntry, resumeEntry.start < json.start, resumeEntry.end >= json.end);
    }

    rows.forEach(yearRow => {
        yearRow.buildInto(table);
    });

    let completeElement = document.createElement('div');
    completeElement.appendChild(table);
    //completeElement.appendChild(infoBox.infoBox);

    return completeElement;
}

export function generateResume():HTMLElement {

    let infoBox = generateInfoBox();
    let resumeContainer = document.createElement('div');

    fetch('resources/resume.json', {cache: "reload"}).then(result => result.json()).then(json => {
        let resumeTable = generateResumeTableHTML(json, infoBox);
        resumeContainer.appendChild(resumeTable);
        resumeContainer.appendChild(infoBox.infoBox);
    });

    return resumeContainer;
}