import { generateProjects } from "./projects";
import { demoResume, generateResume } from "./resume";
import { getSkillSetList } from "./skills";
import { addRow, clearColumn } from "./visible-content";

class Tabbar {

    private tabbar:HTMLElement;
    private tablist:HTMLUListElement;

    private tabUnderscores:HTMLElement[];

    constructor() {
        this.tabbar = document.createElement('div');
        this.tabbar.setAttribute('id', 'tab-bar');

        this.tablist = document.createElement('ul');
        this.tabUnderscores = [];

        this.tabbar.appendChild(this.tablist);
    }

    public addTab(title:string, color:string, content:HTMLElement) {
        let tab = document.createElement('li');
        tab.classList.add('noselect');

        let tabTitle = document.createElement('div');
        tabTitle.innerText = title;

        let tabUnderscore = document.createElement('div');
        tabUnderscore.classList.add('underscore');
        tabUnderscore.style.backgroundColor = color;

        this.tabUnderscores.push(tabUnderscore);

        tab.appendChild(tabTitle);
        tab.appendChild(tabUnderscore);

        tab.addEventListener('click', () => {
            for (const tabUnderscore of this.tabUnderscores) {
                tabUnderscore.classList.remove('active');
            }
            tabUnderscore.classList.add('active');
            clearColumn();
            addRow(content);
        });

        this.tablist.appendChild(tab);
    }

    public open(index:number) {
        (this.tablist.children[index] as HTMLElement).click();
    }

    public buildTabbarInto(element:HTMLElement) {
        element.appendChild(this.tabbar);
    }
}

const tabbar = new Tabbar();
const skillsContent = getSkillSetList();
const resumeContent = generateResume();
const projectsContent = generateProjects();

tabbar.addTab('projects', '#bf0', projectsContent);
tabbar.addTab('skills', '#b0f', skillsContent);
tabbar.addTab('work & education', '#ffa700', resumeContent);

tabbar.buildTabbarInto(document.getElementById('tab-bar-container')!!)
tabbar.open(0);