
type Skill = {
    title:string,
    img:string
}

type SkillSet = {
    title:string;
    skills:Skill[]
}

function generateSkillSetHTML(json:SkillSet):HTMLElement {
    let skillSet = document.createElement('div');
    skillSet.classList.add('skill-set');

    let skillSetTitle = document.createElement('div');
    skillSetTitle.classList.add('skill-set-title');
    skillSetTitle.innerText = json.title;

    let skillSetList = document.createElement('div');
    skillSetList.classList.add('skill-set-list');

    for (const s of json.skills) {
        let skill = document.createElement('img');
        skill.setAttribute('src', s.img);
        skill.setAttribute('title', s.title);
        skill.setAttribute('alt', s.title);

        skillSetList.appendChild(skill);
    }

    skillSet.appendChild(skillSetTitle);
    skillSet.appendChild(skillSetList);

    return skillSet;
}

/*export let skillSetList = document.createElement('div');
skillSetList.setAttribute('id', 'skill-list');

fetch('resources/skills.json').then(result => result.json()).then(json => {
    skillSetList.innerHTML = "";
    json.forEach((skillSet:any) => {
        let htmlSkillSet = generateSkillSetHTML(skillSet);
        skillSetList.appendChild(htmlSkillSet);
    });
});*/

export function getSkillSetList():HTMLElement {
    let skillSetList = document.createElement('div');
    skillSetList.setAttribute('id', 'skill-list');

    fetch('resources/skills.json', {cache: "reload"}).then(result => result.json()).then(json => {
        skillSetList.innerHTML = "";
        json.forEach((skillSet:any) => {
            let htmlSkillSet = generateSkillSetHTML(skillSet);
            skillSetList.appendChild(htmlSkillSet);
        });
    });

    return skillSetList;
}